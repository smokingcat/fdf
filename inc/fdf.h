/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/29 10:47:13 by vde-chab          #+#    #+#             */
/*   Updated: 2015/01/08 12:26:22 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H
# include "../src/libft/libft.h"
# include "../inc/get_next_line.h"
# include <fcntl.h>
# include <mlx.h>
# define WIDTH 2560
# define HEIGHT 1440

typedef struct		s_line
{
	int				x1;
	int				y1;
	int				x2;
	int				y2;
}					t_line;

typedef struct		s_draw
{
	int				dx;
	int				dy;
	int				sx;
	int				sy;
	int				err;
	int				e2;
}					t_draw;

typedef struct		s_point
{
	int				x;
	int				y;
	int				z;
	struct s_point	*next;
}					t_point;

typedef struct		s_data
{
	void			*mlx;
	void			*win;
	int				max_x;
	int				max_y;
	int				min_z;
	int				max_z;
	int				**tab;
	t_point			*begin_line;
	t_point			*tmp;
	t_line			*line;
}					t_data;

void				error(char *str);
int					count_x(char *line);
int					**ft_alloc_tab(int max_x, int max_y);
void				get_tab_from_line(char *line, int *tab);
int					**get_tab(char *file_name, int max_x, int max_y);
t_data				get_nbr(char *file_name);
int					draw_line(t_line *line, t_data *tab, int color);
t_draw				*ft_init_draw(t_line *line);
t_line				*ft_init_line(int x1, int y1, int x2, int y2);
t_point				*ft_init_point(int x, int y);
void				draw(t_data *tab);
int					cte_x(int x, int y, float cte1, float cte2);
int					cte_y(int x, int y, float cte2, int z);
void				get_max_min_z(t_data *tab);
void				multiply(t_data *tab);
void				error(char *str);
#endif
