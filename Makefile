# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/01/06 15:03:36 by vde-chab          #+#    #+#              #
#    Updated: 2015/01/08 11:33:40 by vde-chab         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #
WFLAGS		=  -Wall -Wextra -Werror

LFLAGS		=  -L/usr/X11/lib -lmlx -lXext -lX11

NAME		=fdf

SRC			=src/fdf.c			\
			src/get_next_line.c \
			src/line.c			\
			src/point.c			\
			src/get_nbr.c		\
			src/drawing_box.c	\
			src/multiply.c		\
			src/error.c			\

OBJ			=$(SRC:.c=.o)

.PHONY:		all clean fclean re

all:		$(NAME)

clean:
			@/bin/rm -f $(OBJ)
			@echo "objects cleaned"

fclean:		clean
			@/bin/rm -f $(NAME)
			@echo "$(NAME) cleaned"

$(NAME):	$(OBJ)
			@make -C src/libft/ > /dev/null
			@make -C ressources/minilibx/ > /dev/null
			@$(CC) $(WFLAGS) $(LFLAGS) -o $(NAME) $(OBJ) \
			ressources/minilibx/libmlx.a src/libft/libft.a
			@echo "$(NAME) done"

%.o: %.c
			@$(CC) $(WFLAGS) -c $<
			@mv *.o src/
			@echo "$< compiled"

re:			fclean all
