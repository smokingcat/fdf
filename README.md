# FDF   

FDF (Fil de Fer) is a program written in C that make an isometrical 3D representation of a coordinates map.

* git clone https://smokingcat@bitbucket.org/smokingcat/fdf.git
* cd fdf
* Compile with make
* Execute with ./fdf [map.fdf]

A basic map would look like this:



```sh
0 0 10 5 0 60 5
0 4 50 5 0 35 0
0 25 0 2 1 41 4
4 75 5 -60 4 45
```

![Alt text](http://i.imgur.com/PFlLNRD.png)

![Alt text](http://i.imgur.com/zy7w4xk.png)

![Alt text](http://i.imgur.com/BJST7ii.png)