/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 15:14:55 by vde-chab          #+#    #+#             */
/*   Updated: 2015/01/07 18:38:37 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/fdf.h"

int		cte_x(int x, int y, float cte1, float cte2)
{
	return (cte1 * x - cte2 * y);
}

int		cte_y(int x, int y, float cte2, int z)
{
	return (z + ((cte2 / 2) / 2) * x + (cte2 / 2) * y);
}

int		expose_hook(t_data *tab)
{
	draw(tab);
	return (0);
}

int		key_hook(int keycode)
{
	if (keycode == 65307)
		exit(0);
	return (0);
}

int		main(int ac, char **av)
{
	t_data tab;

	if (ac != 2)
		error("wrong number of arguments\n");
	tab = get_nbr(av[1]);
	tab.mlx = mlx_init();
	tab.win = mlx_new_window(tab.mlx, WIDTH, HEIGHT, av[1]);
	mlx_expose_hook(tab.win, expose_hook, &tab);
	mlx_key_hook(tab.win, key_hook, &tab);
	mlx_loop(tab.mlx);
	return (0);
}
