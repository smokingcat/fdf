/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_nbr.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/07 18:51:03 by vde-chab          #+#    #+#             */
/*   Updated: 2015/01/07 18:51:41 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/fdf.h"

int		count_x(char *line)
{
	int x;
	int i;

	x = 0;
	i = 0;
	while (line[i] || line[i] == '\n')
	{
		if (line[i] == '-')
		{
			i++;
			if (!ft_isdigit(line[i]))
				error("map error.\n");
		}
		x++;
		while (ft_isdigit(line[i]))
			i++;
		while (line[i] == ' ')
			i++;
	}
	return (x);
}

int		**ft_alloc_tab(int max_x, int max_y)
{
	int	**tab;
	int y;

	y = 0;
	tab = malloc(sizeof(*tab) * (max_y + 1));
	if (!tab)
		error("malloc error");
	while (y <= max_y)
	{
		tab[y] = malloc(sizeof(**tab) * (max_x + 1));
		if (!tab[y])
			error("malloc error");
		y++;
	}
	return (tab);
}

void	get_tab_from_line(char *line, int *tab)
{
	int i;
	int	x;

	i = 0;
	x = 0;
	while (line[i])
	{
		while (line[i] == ' ')
			i++;
		if (ft_isdigit(line[i]) || line[i] == '-')
		{
			tab[x] = ft_atoi(line + i);
			x++;
			while (line[i] == '-' || ft_isdigit(line[i]))
				i++;
		}
	}
}

int		**get_tab(char *file_name, int max_x, int max_y)
{
	int		**tab;
	int		fd;
	char	*line;
	int		x;

	x = 0;
	tab = ft_alloc_tab(max_x, max_y);
	fd = open(file_name, O_RDONLY);
	if (fd <= 0)
		error("invalid file descriptor\n");
	while (get_next_line(fd, &line) > 0)
	{
		get_tab_from_line(line, tab[x]);
		x++;
	}
	return (tab);
}

t_data	get_nbr(char *file_name)
{
	char	*line;
	int		fd;
	int		ret;
	t_data	tab;

	fd = open(file_name, O_RDONLY);
	if (fd <= 0)
		error("invalid file descriptor\n");
	if ((ret = get_next_line(fd, &line)))
		tab.max_x = count_x(line);
	if (ret == -1)
		error("get_next_line couldn't get a line");
	tab.max_y = 1;
	while (get_next_line(fd, &line) > 0)
		tab.max_y++;
	close(fd);
	tab.tab = get_tab(file_name, tab.max_x, tab.max_y);
	return (tab);
}
