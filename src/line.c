/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   line.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/29 12:43:52 by vde-chab          #+#    #+#             */
/*   Updated: 2015/01/08 12:25:18 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/fdf.h"

t_line	*ft_init_line(int x1, int y1, int x2, int y2)
{
	t_line *line;

	line = malloc(sizeof(t_line));
	if (line)
	{
		line->x1 = x1;
		line->y1 = y1;
		line->x2 = x2;
		line->y2 = y2;
	}
	return (line);
}

t_draw	*ft_init_draw(t_line *line)
{
	t_draw *draw;

	draw = malloc(sizeof(t_draw));
	if (draw)
	{
		draw->dx = (line->x2 - line->x1 < 0) ? \
					-(line->x2 - line->x1) : line->x2 - line->x1;
		draw->sx = line->x1 < line->x2 ? 1 : -1;
		draw->dy = (line->y2 - line->y1 < 0) ? \
					-(line->y2 - line->y1) : line->y2 - line->y1;
		draw->sy = line->y1 < line->y2 ? 1 : -1;
		draw->err = (draw->dx > draw->dy ? draw->dx : -(draw->dy)) / 2;
	}
	return (draw);
}

int		draw_line(t_line *line, t_data *tab, int color)
{
	t_draw *draw;

	draw = ft_init_draw(line);
	while (42)
	{
		mlx_pixel_put(tab->mlx, tab->win, line->x1, line->y1, color);
		if (line->x1 == line->x2 && line->y1 == line->y2)
			return (42);
		draw->e2 = draw->err;
		if (draw->e2 > -(draw->dx))
		{
			draw->err -= draw->dy;
			line->x1 += draw->sx;
		}
		if (draw->e2 < draw->dy)
		{
			draw->err += draw->dx;
			line->y1 += draw->sy;
		}
	}
}
