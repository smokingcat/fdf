/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   multiply.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/07 18:25:11 by vde-chab          #+#    #+#             */
/*   Updated: 2015/01/07 18:28:21 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/fdf.h"

void	get_max_min_z(t_data *tab)
{
	int i;
	int j;

	i = 0;
	j = 0;
	tab->max_z = 0;
	tab->min_z = 0;
	while (j < tab->max_y)
	{
		while (i < tab->max_x)
		{
			if (tab->tab[j][i] > tab->max_z)
				tab->max_z = tab->tab[j][i];
			else if (tab->tab[j][i] < tab->min_z)
				tab->min_z = tab->tab[j][i];
			i++;
		}
		i = 0;
		j++;
	}
}

void	multiply(t_data *tab)
{
	int i;
	int j;
	int multiply;

	i = 0;
	j = 0;
	multiply = 1;
	if (tab->max_z <= 40 && tab->min_z >= -40)
		multiply = 3;
	if (tab->max_z < 10 && tab->min_z > -10)
		multiply = 10;
	while (j < tab->max_y)
	{
		while (i < tab->max_x)
		{
			tab->tab[j][i] *= multiply;
			i++;
		}
		i = 0;
		j++;
	}
}
