/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/04 15:50:09 by vde-chab          #+#    #+#             */
/*   Updated: 2014/12/03 12:11:49 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_strcmp(const char *s1, const char *s2)
{
	unsigned char	*p_s1;
	unsigned char	*p_s2;
	int				s1_len;

	p_s1 = (unsigned char *)s1;
	p_s2 = (unsigned char *)s2;
	s1_len = ft_strlen((char *)p_s1);
	if (!s1_len && !ft_strlen((char *)p_s2))
		return (0);
	if (!*s1 || s1_len < (signed int)ft_strlen((char *)p_s2))
		return (-1);
	while (s1_len--)
	{
		if (*p_s1 != *p_s2)
		{
			if (*p_s1 > *p_s2)
				return (1);
			else
				return (-1);
		}
		p_s1++;
		p_s2++;
	}
	return (0);
}
