/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/04 18:49:29 by vde-chab          #+#    #+#             */
/*   Updated: 2014/11/04 19:45:09 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_atoi(const char *str)
{
	int		result;
	int		sign;
	int		i;
	char	*p_str;

	result = 0;
	sign = 1;
	i = 0;
	p_str = (char *)str;
	while (p_str[i] != '\0' && ((p_str[i] >= 9 && p_str[i] <= 13) \
				|| p_str[i] == ' '))
		i++;
	if (p_str[i] == '-')
	{
		sign = -1;
		i++;
	}
	else if (p_str[i] == '+')
		i++;
	while (p_str[i] != '\0' && p_str[i] >= '0' && p_str[i] <= '9')
	{
		result = result * 10 + p_str[i] - '0';
		i++;
	}
	return (result * sign);
}
