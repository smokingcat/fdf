/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/04 16:38:37 by vde-chab          #+#    #+#             */
/*   Updated: 2014/12/02 13:59:49 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int ft_strncmp(const char *s1, const char *s2, size_t n)
{
	unsigned char *p_s1;
	unsigned char *p_s2;

	p_s1 = (unsigned char *)s1;
	p_s2 = (unsigned char *)s2;
	while (*p_s1 && n && *p_s1 == *p_s2)
	{
		++p_s1;
		++p_s2;
		--n;
	}
	if (!n)
		return (0);
	if ((*p_s1 - *p_s2) > 0)
		return (1);
	else if ((*p_s1 - *p_s2) < 0)
		return (-1);
	else
		return (0);
}
