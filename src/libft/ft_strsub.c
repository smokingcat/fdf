/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/06 13:18:58 by vde-chab          #+#    #+#             */
/*   Updated: 2014/11/11 14:46:43 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

char *ft_strsub(char const *s, unsigned int start, size_t len)
{
	int		i;
	char	*sub;

	i = 0;
	if ((int)len < 0)
		return (NULL);
	sub = (char *)malloc(sizeof(char) * (len + 1));
	if (s != NULL && (start != 0 || len != 0) && sub)
	{
		if (!sub)
			return (0);
		while (len--)
		{
			*sub = s[start++];
			sub++;
			i++;
		}
	}
	*sub = '\0';
	return (sub - i);
}
