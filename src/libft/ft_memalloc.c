/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 18:10:06 by vde-chab          #+#    #+#             */
/*   Updated: 2014/11/28 15:13:21 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

void *ft_memalloc(size_t size)
{
	void *area;
	char *p_area;

	area = (void *)malloc(size);
	p_area = (char *)area;
	if (area != NULL)
		while (size--)
			*p_area++ = 0;
	return (area);
}
