/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/04 15:27:01 by vde-chab          #+#    #+#             */
/*   Updated: 2014/11/08 20:31:10 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t	keep_size;
	size_t	dst_len;
	size_t	src_len;

	keep_size = size;
	dst_len = ft_strlen(dst);
	src_len = ft_strlen(src);
	while (*dst && size)
	{
		dst++;
		size--;
	}
	if (!size)
		return (keep_size + src_len);
	while (*src && size-- > 1)
		*dst++ = *src++;
	*dst = '\0';
	return (dst_len + src_len);
}
