/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/11 14:57:05 by vde-chab          #+#    #+#             */
/*   Updated: 2014/11/11 15:01:14 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

static int	ft_wordcount(char *s, char c)
{
	int i;
	int word;

	word = 0;
	i = 0;
	if (s)
	{
		while (s[i] == c)
			s++;
		while (s[i])
		{
			if (i == 0 && s[i] != c)
				word++;
			else if (s[i] != c && s[i - 1] == c)
				word++;
			i++;
		}
	}
	return (word);
}

char		**ft_strsplit(char const *s, char c)
{
	int		i;
	char	**tab;
	int		nb_words;
	int		word_len;

	i = 0;
	nb_words = ft_wordcount((char *)s, c);
	if ((tab = (char **)malloc(nb_words * sizeof(char *) + 1)))
	{
		word_len = 0;
		while (i < nb_words)
		{
			while (*s == c)
				s++;
			while (*s != c && *s)
			{
				word_len++;
				s++;
			}
			tab[i++] = ft_strsub(s - word_len, 0, word_len);
			word_len = 0;
		}
		tab[i] = NULL;
	}
	return (tab);
}
