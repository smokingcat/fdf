/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 15:31:13 by vde-chab          #+#    #+#             */
/*   Updated: 2014/12/03 12:17:14 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	if (n && s)
		while (n--)
		{
			if ((unsigned char)c == *(unsigned char *)s)
				return ((void *)s);
			else
				s++;
		}
	return (NULL);
}
