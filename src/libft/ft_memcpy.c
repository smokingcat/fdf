/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/03 13:21:06 by vde-chab          #+#    #+#             */
/*   Updated: 2014/11/06 17:08:39 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void *ft_memcpy(void *dest, const void *src, size_t n)
{
	char *destination;
	char *source;

	source = (char *)src;
	destination = (char *)dest;
	while (n--)
	{
		destination[0] = source[0];
		++destination;
		++source;
	}
	return (dest);
}
