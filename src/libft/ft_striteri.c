/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_striteri.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/06 14:40:19 by vde-chab          #+#    #+#             */
/*   Updated: 2014/11/08 20:27:10 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void ft_striteri(char *s, void (*f)(unsigned int, char *))
{
	unsigned int	i;
	char			*tmp;

	if (s != NULL && f != NULL)
	{
		i = 0;
		tmp = s;
		while (*(tmp + i))
		{
			f(i, tmp + i);
			i++;
		}
	}
}
