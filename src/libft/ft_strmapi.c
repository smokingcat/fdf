/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/08 20:32:48 by vde-chab          #+#    #+#             */
/*   Updated: 2014/11/08 20:33:34 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	unsigned int	i;
	unsigned int	s_len;
	char			*s_copy;

	if (s == NULL || f == NULL || !(*s))
		return (NULL);
	i = 0;
	s_len = ft_strlen(s);
	s_copy = ft_strdup(s);
	if (!s_copy)
		return (NULL);
	while (i < s_len)
	{
		*(s_copy) = f(i, (*s_copy));
		i++;
		s_copy++;
	}
	return (s_copy - i);
}
