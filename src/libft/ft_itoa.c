/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/11 13:16:31 by vde-chab          #+#    #+#             */
/*   Updated: 2014/11/11 14:50:20 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static unsigned int	absolute(int n)
{
	unsigned int absolute_n;

	if (n == -2147483648)
		absolute_n = 2147483648;
	if (n < 0)
		n = -n;
	absolute_n = (unsigned int)n;
	return (absolute_n);
}

static int			size_n(unsigned int nb)
{
	int	i;

	i = 0;
	while (nb >= 1)
	{
		nb = nb / 10;
		i++;
	}
	return (i);
}

char				*ft_itoa(int n)
{
	int				i;
	char			*s;
	unsigned int	absolute_n;

	absolute_n = absolute(n);
	i = size_n(absolute_n);
	if (n < 0)
		i++;
	s = (char *)malloc(sizeof(*s) * (i + 1));
	if (!s)
		return (NULL);
	if (absolute_n == 0)
		s[i++] = '0';
	s[i] = '\0';
	while (i)
	{
		i--;
		s[i] = ((absolute_n % 10) + '0');
		absolute_n = absolute_n / 10;
	}
	if (n < 0)
		*s = '-';
	return (s);
}
