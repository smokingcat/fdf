/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/21 15:23:36 by vde-chab          #+#    #+#             */
/*   Updated: 2015/01/07 18:10:12 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/get_next_line.h"

char	*ft_offset_to(char *s1, char *to_free)
{
	char *str;

	str = ft_strdup(s1);
	if (!str)
		return (NULL);
	if (to_free)
		ft_strdel(&to_free);
	return (str);
}

void	update_line_and_free(char **line, char *str, char *buffer)
{
	*line = ft_strdup(str);
	if (str)
		ft_strdel(&str);
	if (buffer)
		ft_strdel(&buffer);
}

int		get_next_line(int const fd, char **line)
{
	static char *str = NULL;
	static char *buffer = NULL;
	int			lu;
	char		*line_break;

	if (fd < 0 || line == NULL)
		return (-1);
	buffer = ft_strnew(BUFF_SIZE + 1);
	str = (!str) ? ft_strnew(1) : str;
	while ((ft_strchr(str, '\n') == NULL))
	{
		lu = read(fd, buffer, BUFF_SIZE);
		if (lu <= 0)
		{
			update_line_and_free(line, str, buffer);
			return (ft_strlen(buffer) ? 1 : lu);
		}
		buffer[lu] = '\0';
		str = ft_strjoin(str, (char *)buffer);
	}
	line_break = ft_strchr(str, '\n');
	*line_break = '\0';
	*line = ft_strdup(str);
	str = ft_offset_to(line_break + 1, str);
	return (1);
}
