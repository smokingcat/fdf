/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   drawing_box.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 17:47:54 by vde-chab          #+#    #+#             */
/*   Updated: 2015/01/08 12:25:55 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/fdf.h"

void	draw_list(t_data *tab, int color)
{
	tab->tmp = tab->begin_line;
	while (tab->tmp->next)
	{
		tab->line = ft_init_line(\
		tab->tmp->x, tab->tmp->y, tab->tmp->next->x, tab->tmp->next->y);
		draw_line(tab->line, tab, color);
		tab->tmp = tab->tmp->next;
	}
}

void	draw_y(t_data *tab, int x, int y)
{
	int i;
	int j;

	i = 0;
	j = 0;
	while (i < tab->max_y)
	{
		tab->begin_line = ft_init_point(\
		cte_x(x, y, 0.7, 0.7), cte_y(x, y, 1, -tab->tab[i][j]));
		j++;
		tab->tmp = tab->begin_line;
		while (j < tab->max_x)
		{
			x += (WIDTH / tab->max_x) / 2;
			tab->tmp->next = ft_init_point(\
			cte_x(x, y, 0.7, 0.7), cte_y(x, y, 1, -tab->tab[i][j]));
			tab->tmp = tab->tmp->next;
			j++;
		}
		draw_list(tab, 2552555255);
		x = WIDTH / tab->max_x + (WIDTH / 2);
		y += (HEIGHT / tab->max_y) / 2;
		i++;
		j = 0;
	}
}

void	draw_x(t_data *tab, int x, int y)
{
	int i;
	int j;

	i = 0;
	j = 0;
	while (j < tab->max_x)
	{
		tab->begin_line = ft_init_point(\
		cte_x(x, y, 0.7, 0.7), cte_y(x, y, 1, -tab->tab[i][j]));
		i++;
		tab->tmp = tab->begin_line;
		while (i < tab->max_y)
		{
			y += (HEIGHT / tab->max_y) / 2;
			tab->tmp->next = ft_init_point(\
			cte_x(x, y, 0.7, 0.7), cte_y(x, y, 1, -tab->tab[i][j]));
			tab->tmp = tab->tmp->next;
			i++;
		}
		draw_list(tab, 255255255);
		x += (WIDTH / tab->max_x) / 2;
		y = HEIGHT / tab->max_y;
		j++;
		i = 0;
	}
}

void	draw(t_data *tab)
{
	int x;
	int y;

	get_max_min_z(tab);
	multiply(tab);
	x = WIDTH / tab->max_x + (WIDTH / 2);
	y = HEIGHT / tab->max_y;
	draw_x(tab, x, y);
	draw_y(tab, x, y);
}
