/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   point.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/30 10:26:07 by vde-chab          #+#    #+#             */
/*   Updated: 2015/01/08 12:26:14 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/fdf.h"

t_point	*ft_init_point(int x, int y)
{
	t_point *point;

	point = malloc(sizeof(t_point));
	if (point)
	{
		point->x = x;
		point->y = y;
		point->next = NULL;
	}
	return (point);
}
